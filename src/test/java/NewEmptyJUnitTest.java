/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mongodb.MongoException;
import com.mongodb.client.MongoDatabase;
import dades.DadesException;
import dades.model.CredencialsBD;
import dades.model.Jugador;
import static dades.mongoDB.JugadorsBD.CarregarJugadorsByIdEquip;
import static dades.mongoDB.JugadorsBD.eliminarJugadors;
import static dades.mongoDB.JugadorsBD.insertJugadorBD;
import static dades.mongoDB.JugadorsBD.modificarJugador;
import dades.mongoDB.MongoDriver;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;
import static dades.mongoDB.JugadorsBD.carregarJugadors;


/**
 *
 * @author manel
 */
public class NewEmptyJUnitTest {
    
    @Test
    public void Test1(){
        
        CredencialsBD cr = new CredencialsBD();
        cr.setBD("bd1");
        
        try {
            MongoDatabase db = MongoDriver.ConnectarBD(cr);
            
            db.getCollection("col1");
            
            Jugador j1 = new Jugador();
            
            j1.setId(23);
            j1.setNombre("Pepe");
            j1.setDorsal(22);
            j1.setEdad(33);
            j1.setIdEquip(2);
            j1.setCp("12345");
            
            insertJugadorBD(db, j1);
            
            j1.setNombre("Juan");
            
            modificarJugador(db, j1);
            
            ArrayList<Jugador> arr = CarregarJugadorsByIdEquip(db, 2);
            
            arr.size();
            
        } catch (DadesException ex) {
            Logger.getLogger(NewEmptyJUnitTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MongoException ex) {
            Logger.getLogger(NewEmptyJUnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
