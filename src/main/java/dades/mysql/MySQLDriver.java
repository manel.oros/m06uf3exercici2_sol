/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dades.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import dades.model.CredencialsBD;

/**
 * Classe que facilita la connexió a un servidor MySQL
 * @author manel
 */
public class MySQLDriver {
    
     /**
     * Connecta a una BD mysql 
     * @param cr onjecte credencials amb les dades de connexió
     * @return objecte Connection
     * @throws SQLException 
     */
    public static Connection ConnectarBD(CredencialsBD cr) throws SQLException
    {
        String bd = cr.getBD();
        String usuari = cr.getUser();
        String password = cr.getPassword();
        
        Connection ret = null;
        
         ret =  DriverManager.getConnection("jdbc:mysql://localhost:3306/"+bd+"?useUnicode=true&"
                            + "useJDBCCompliantTimezoneShift=true&"
                            + "useLegacyDatetimeCode=false&serverTimezone=UTC", usuari, password);
        
        return ret;
    }
    
}
