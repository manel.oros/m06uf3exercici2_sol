/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dades.model;

import java.util.ArrayList;

/**
 * Modela un conjunt d'equips.
 * @author manel
 */
public class Equips {
    
    private ArrayList<Equip> equips = new ArrayList<>();

    public Equips() {
    }

    public ArrayList<Equip> getEquips() {
        return equips;
    }

    public void setEquips(ArrayList<Equip> equips) {
        this.equips = equips;
    }
}
