/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dades.model;

/**
 * Classe que conté les credencials per a connectar a la BBDD
 * @author manel
 */
public class CredencialsBD {
    
    String user;
    String password;
    String BD;

    public CredencialsBD() {
    }

    public CredencialsBD(String user, String password, String BD) {
        this.user = user;
        this.password = password;
        this.BD = BD;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBD() {
        return BD;
    }

    public void setBD(String BD) {
        this.BD = BD;
    }
    
}
