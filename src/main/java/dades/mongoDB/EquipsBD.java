/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dades.mongoDB;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import dades.DadesException;
import dades.model.Equip;
import java.sql.SQLException;
import java.util.ArrayList; 
import org.bson.Document;

/**
 *
 * @author manel
 */
public class EquipsBD {
    
    static final String COL = "equips";
    
    /**
     * Elimina tots els equips 
     * @param con
     * @param e
     * @throws SQLException
     */
    public static void deleteAllEquipsBD(MongoDatabase con) throws DadesException
    {
        MongoCollection<Document> coleccio = con.getCollection(COL);
         
         try{
             
             // Eliminem tot
             coleccio.deleteMany(new Document());
                    
        }catch(MongoException ex){
            throw new DadesException("Error eliminant equips: " + ex.toString());
        }
    }
    
     /**
     * Retorna el contingut de la taula en una col.lecció de equips (sense jugadors per equip)
     * @param con
     * @return 
     */
    public static ArrayList<Equip> getEquipsBD(MongoDatabase con) throws DadesException
    {
        ArrayList<Equip> ret = new ArrayList<>();
        
        MongoCollection<Document> coleccio = con.getCollection(COL);
         
        try{
             
             // Carreguem jugadors
             MongoCursor<Document> cur = coleccio.find().cursor();
             
             // convertim a entitat Jugador
             while (cur.hasNext()) ret.add(MappingEquip.FromDoc(cur.next()));
             
        }catch(MongoException ex){
            throw new DadesException("Error carregant equips: " + ex.toString());
        }
        
        return ret;
    }
    
    /**
     * Elimina l'equip amb l'identificador subministrat
     * @param con
     * @param e
     * @throws SQLException 
     */
    public static void deleteEquipBD(MongoDatabase con, Equip e) throws DadesException
    {
       MongoCollection<Document> coleccio = con.getCollection(COL);
         
         try{             
             
             // Eliminem equip pel seu id
              DeleteResult r = coleccio.deleteOne(new Document(MappingJugador.ID, e.getId()));
             
             if (r.getDeletedCount() == 0)
                  throw new DadesException("No s'ha pogut eliminar l'equip " + e);
                    
        }catch(MongoException ex){
            throw new DadesException("Error eliminant equip: " + ex.toString());
        }
    }
    
    /**
     * Insereix un nou equip amb les dades subministrades
     * Ignora l'id de l'equip i suma 1 al darrer id
     * @param con
     * @param e
     * @throws SQLException 
     */
    public static void insertEquipBD(MongoDatabase con, Equip e) throws DadesException
    {
        MongoCollection<Document> coleccio = con.getCollection(COL);
         Integer id = 0;
         
         try{
               
             // obtenim el següent id lliure
             // al utilitzar el camp _id com a id d'equip, ja és indexat i únic per defecte
             Document d = coleccio.find().sort(new Document(MappingEquip.ID, -1)).first();
             
             // si la colecció no és buida, agafem el darrer document
             if (d != null) id = MappingEquip.FromDoc(d).getId();
             
             // incrementem el seu id
             id++;
             
             // i l'assignem al nou jugador
             e.setId(id);
             
             // desem el nou jugador
             coleccio.insertOne(MappingEquip.toDoc(e));
                    
        }catch(MongoException ex){
            throw new DadesException("Error inserint equip: " + ex.toString());
        }
    }
    
    /**
     * Modifica un equip si existeix
     * @param con
     * @param e
     * @throws dades.DadesException
     * @throws SQLException 
     */
    public static void modifyEquipBD(MongoDatabase con, Equip e) throws DadesException
    {
         MongoCollection<Document> coleccio = con.getCollection(COL);
         
         try{
                // cerca per PK
                BasicDBObject searchQuery = new BasicDBObject(dades.mongoDB.MappingEquip.ID, e.getId());
                
                // objecte modificat
                BasicDBObject updateFields = new BasicDBObject();
                updateFields.append(dades.mongoDB.MappingEquip.CP, e.getCp());
                updateFields.append(dades.mongoDB.MappingEquip.ESTADIO, e.getEstadio());
                updateFields.append(dades.mongoDB.MappingEquip.NOM, e.getNombre());
                updateFields.append(dades.mongoDB.MappingEquip.POBLACION, e.getPoblacion());
                updateFields.append(dades.mongoDB.MappingEquip.PROVINCIA, e.getProvincia());
                
                // query per update
                BasicDBObject setQuery = new BasicDBObject();
                setQuery.append("$set", updateFields);
                
                // execució de la query
                UpdateResult updateResult = coleccio.updateOne(searchQuery, setQuery);
                
                // verificació de resultats
                if (updateResult.getMatchedCount() == 0)
                     throw new DadesException("No s'ha trobat equip: " + e.getId());
           
                    
        }catch(MongoException ex){
            throw new DadesException("Error modifyEquipBD: " + ex.toString());
        }
    }
    
   
}
