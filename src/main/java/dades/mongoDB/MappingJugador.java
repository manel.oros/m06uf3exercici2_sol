/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dades.mongoDB;

import dades.model.Jugador;
import org.bson.Document;

/**
 * Realitza la conversió entre la classe Jugador i documents BSON
 * @author manel
 */
public class MappingJugador {
    
    public static final String ID = "_id";
    public static final String IDEQUIP = "idEquip";
    public static final String NOM = "nombre";
    public static final String DORSAL = "dorsal";
    public static final String EDAD = "edad";
    public static final String CP = "cp";
    
    /***
     * Retorna una estructura BSON
     * @param j
     * @return 
     */
    public static Document toDoc(Jugador j)
    {   
       Document ret = new Document(ID , j.getId())
                    .append(IDEQUIP , j.getIdEquip())
                    .append(NOM , j.getNombre())
                    .append(DORSAL , j.getDorsal())
                    .append(EDAD , j.getEdad())
                    .append(CP , j.getCp());
        
        return ret;
    }
    
    /***
     * Retorna un objecte en base a un document BSON
     * @param d
     * @return 
     */
    public static Jugador FromDoc(Document d)
    {
        Jugador ret = null;
        
        ret = new Jugador();
        
        ret.setId(d.getInteger(ID));
        ret.setIdEquip(d.getInteger(IDEQUIP));
        ret.setNombre(d.getString(NOM));
        ret.setDorsal(d.getInteger(DORSAL));
        ret.setEdad(d.getInteger(EDAD));
        ret.setCp(d.getString(CP));
        
        return ret;
    }
    
}
