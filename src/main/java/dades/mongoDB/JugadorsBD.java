/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dades.mongoDB;


import com.mongodb.BasicDBObject;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import dades.DadesException;
import dades.model.Jugador;
import java.util.ArrayList;
import org.bson.Document;

/**
 *  //https://kb.objectrocket.com/mongo-db/how-to-update-a-document-in-mongodb-using-java-384
 * //https://www.baeldung.com/mongodb-update-multiple-fields
 *
 * @author manel
 */
public class JugadorsBD {
    
    static final String COL = "jugadors";
    
    /**
     * Modifica el jugador amb les dades contingudes
     * Conserva el ID ì substitueix tota la resta
     * @param con
     * @param j 
     * @throws dades.DadesException 
     */
    public static void modificarJugador(MongoDatabase con, Jugador j) throws DadesException
    {
         MongoCollection<Document> coleccio = con.getCollection(COL);
         
         try{
                // cerca per PK
                BasicDBObject searchQuery = new BasicDBObject(dades.mongoDB.MappingJugador.ID, j.getId());
                
                // objecte modificat
                BasicDBObject updateFields = new BasicDBObject();
                updateFields.append(dades.mongoDB.MappingJugador.CP, j.getCp());
                updateFields.append(dades.mongoDB.MappingJugador.DORSAL, j.getDorsal());
                updateFields.append(dades.mongoDB.MappingJugador.EDAD, j.getEdad());
                updateFields.append(dades.mongoDB.MappingJugador.IDEQUIP, j.getIdEquip());
                updateFields.append(dades.mongoDB.MappingJugador.NOM, j.getNombre());
                
                // query per update
                BasicDBObject setQuery = new BasicDBObject();
                setQuery.append("$set", updateFields);
                
                // execució de la query
                UpdateResult updateResult = coleccio.updateOne(searchQuery, setQuery);
                
                // verificació de resultats
                if (updateResult.getMatchedCount() == 0)
                     throw new DadesException("No s'ha trobat el jugador: " + j.getId());
           
                    
        }catch(MongoException ex){
            throw new DadesException("Error modificarJugador: " + ex.toString());
        }
    }
    
    
    /***
     * Elimina tots els jugadors de la BBDD
     * @param con
     * @throws DadesException 
     */
    public static void eliminarJugadors(MongoDatabase con) throws DadesException
    {
         MongoCollection<Document> coleccio = con.getCollection(COL);
         
         try{
             
             // Eliminem tot
             coleccio.deleteMany(new Document());
                    
        }catch(MongoException ex){
            throw new DadesException("Error eliminant Jugadors: " + ex.toString());
        }
    }
   
    
    /**
     * Elimina un jugador
     * @param con
     * @param j jugador amb el id a eliminar
     */
    public static void eliminarJugador(MongoDatabase con, Jugador j) throws DadesException
    {
         MongoCollection<Document> coleccio = con.getCollection(COL);
         
         try{
             
             // Eliminem jugador pel seu id
              DeleteResult r = coleccio.deleteOne(new Document(MappingJugador.ID, j.getId()));
             
             if (r.getDeletedCount() == 0)
                  throw new DadesException("No s'ha pogut eliminar el jugador " + j);
                    
        }catch(MongoException ex){
            throw new DadesException("Error eliminant Jugador: " + ex.toString());
        }
    }
    
    /**
     * Retorna el contingut de la taula jugadors
     * @param con
     * @return 
     */
    public static ArrayList<Jugador> carregarJugadors(MongoDatabase con) throws DadesException
    {
        ArrayList<Jugador> ret = new ArrayList<>();
        
        MongoCollection<Document> coleccio = con.getCollection(COL);
         
        try{
             
             // Carreguem jugadors
             MongoCursor<Document> cur = coleccio.find().cursor();
             
             // convertim a entitat Jugador
             while (cur.hasNext()) ret.add(MappingJugador.FromDoc(cur.next()));
             
        }catch(MongoException ex){
            throw new DadesException("Error carregant Jugadors: " + ex.toString());
        }
        
        return ret;
    }
    
     /**
     * Retorna els jugador d'un determinat equip
     * @param con
     * @param idEquip     * @return 
     * @throws dades.DadesException 
     */
    public static ArrayList<Jugador> CarregarJugadorsByIdEquip(MongoDatabase con, int idEquip) throws DadesException
    {
       ArrayList<Jugador> ret = new ArrayList<>();
        
        MongoCollection<Document> coleccio = con.getCollection(COL);
         
        try{
             
             // Carreguem jugadors
             MongoCursor<Document> cur = coleccio.find(new Document(MappingJugador.IDEQUIP, idEquip)).cursor();
             
             // convertim a entitat Jugador
             while (cur.hasNext()) ret.add(MappingJugador.FromDoc(cur.next()));
             
        }catch(MongoException ex){
            throw new DadesException("Error carregant Jugadors: " + ex.toString());
        }
        
        return ret;
    }
    
    /**
     * Insereix un nou jugador amb les dades subministrades
     * Ignora l'id de l'jugador i el genera autoincrementat
     * @param con
     * @param j
     * @param e 
     */
    public static void insertJugadorBD(MongoDatabase con, Jugador j) throws DadesException
    {
         
         MongoCollection<Document> coleccio = con.getCollection(COL);
         Integer id = 0;
         
         try{
               
             // obtenim el següent id lliure
             // al utilitzar el camp _id com a id de jugador, ja és indexat i únic per defecte
             Document d = coleccio.find().sort(new Document(MappingJugador.ID, -1)).first();
             
             // si la colecció no és buida, agafem el darrer document
             if (d != null) id = MappingJugador.FromDoc(d).getId();
             
             // incrementem el seu id
             id++;
             
             // i l'assignem al nou jugador
             j.setId(id);
             
             // desem el nou jugador
             coleccio.insertOne(MappingJugador.toDoc(j));
                    
        }catch(MongoException ex){
            throw new DadesException("Error inserint Jugador: " + ex.toString());
        }
    }
}
