/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dades.mongoDB;

import dades.model.Equip;
import org.bson.Document;

/**
 *
 * @author manel
 */
public class MappingEquip {
    
    public static final String ID = "_id";
    public static final String NOM = "nombre";
    public static final String ESTADIO  = "estadio";
    public static final String POBLACION = "poblacion";
    public static final String PROVINCIA = "provincia";
    public static final String CP = "cp";
    
    /***
     * Retorna una estructura BSON
     * @param e
     * @return 
     */
    public static Document toDoc(Equip e)
    {
        
        Document ret = new Document(ID , e.getId())
                .append(NOM, e.getNombre())
                .append(ESTADIO, e.getEstadio())
                .append(POBLACION, e.getPoblacion())
                .append(PROVINCIA, e.getProvincia())
                .append(CP, e.getCp());
        
        return ret;
    }
    
    /***
     * Retorna un objecte en base a un document BSON
     * @param j
     * @return 
     */
    public static Equip FromDoc(Document j)
    {
        
        Equip ret = new Equip();
        
        ret.setId(j.getInteger(ID));
        ret.setNombre(j.getString(NOM));
        ret.setEstadio(j.getString(ESTADIO));
        ret.setPoblacion(j.getString(POBLACION));
        ret.setProvincia(j.getString(PROVINCIA));
        ret.setCp(j.getString(CP));
        
        return ret;
    }
    
}
