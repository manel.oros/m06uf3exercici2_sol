/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dades.mongoDB;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoException;
import com.mongodb.client.MongoDatabase;
import java.sql.SQLException;
import dades.model.CredencialsBD;

/**
 * Classe que facilita la connexió a un servidor MongoDB
 * @author manel
 */
public class MongoDriver {
    
     /**
     * Connecta a una BD Mongo 
     * 
     * ULL!! SENSE AUTENTICACIÓ. Ignora les credencials subministrades.
     * 
     * //TODO: Afegir autenticació
     * 
     * @param cr onjecte credencials amb les dades de connexió
     * @return objecte Connection
     * @throws SQLException 
     */
    public static MongoDatabase ConnectarBD(CredencialsBD cr) throws MongoException
    {
        String bd = cr.getBD();
        String usuari = cr.getUser();
        String password = cr.getPassword();
        
        MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
        MongoClient mongoClient = new MongoClient(connectionString);
        MongoDatabase bbdd = mongoClient.getDatabase(bd);
        
        return bbdd;
    }
}
