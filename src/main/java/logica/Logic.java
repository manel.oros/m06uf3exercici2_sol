/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import com.mongodb.client.MongoDatabase;
import dades.DadesException;
import java.sql.Connection;
import main.App;

/**
 * Generalització de la capa lógica
 * @author manel
 */
public abstract class Logic {
    
        
    // Defineix si la BBDD és de tipus MYSQL o MongoDB
    // TODO: fer-ho dinàmic !!
    Boolean isMongoDB = false;
    
    /***
     * Obté l'objecte connexió del manager o bé una excepció si aquesta és null
     * @return 
     */
    public static Connection verificarConnexioSQL() throws DadesException
    {
        Connection ret = App.getManager().getConnSQL();
        
        if (ret == null)
            throw new DadesException("L'objecte connexió SQL no és vàlid.");
        
        return ret;
    }
    
    public static MongoDatabase verificarConnexioNoSQL() throws DadesException
    {
        MongoDatabase ret = App.getManager().getConnNoSQL();
        
        if (ret == null)
            throw new DadesException("L'objecte connexió MongoDB no és vàlid.");
        
        return ret;
    }
    
}
