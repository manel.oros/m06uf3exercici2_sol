/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import com.mongodb.client.MongoDatabase;
import main.AplicacioException;
import main.App;
import dades.model.Equip;
import dades.DadesException;
import dades.mysql.JugadorsBD;
import java.sql.Connection;
import java.util.ArrayList;

/**
 *
 * @author manel
 */
public class LogicEquip extends Logic{
    
    /**
     * Retorna tots els equips amb els seus jugadors
     * @return
     * @throws AplicacioException 
     */
    public static ArrayList<Equip> getEquips() throws AplicacioException
    {
        ArrayList<Equip> ret = null;
        
        try {
            if (App.getManager().isMongoDB())
            {
                MongoDatabase conn = verificarConnexioNoSQL();
                ret = dades.mongoDB.EquipsBD.getEquipsBD(conn);
                for (Equip e : ret)
                {
                    e.setJugadors(dades.mongoDB.JugadorsBD.CarregarJugadorsByIdEquip(conn, e.getId()));
                }
            }
            else
            {
                Connection conn = verificarConnexioSQL();
                ret = dades.mysql.EquipsBD.getEquipsBD(conn);
                for (Equip e : ret)
                {
                    e.setJugadors(JugadorsBD.CarregarJugadorsByIdEquip(conn, e.getId()));
                }
            }
        }catch (DadesException ex) {
            throw new AplicacioException("Error a " + ex.getStackTrace()[0].getMethodName() + ":" + ex.toString());
        }
        
        return ret;
    }
    
    /***
     * Insereix un nou equip
     * @param e
     * @throws AplicacioException 
     */
    public static void insertEquip(Equip e) throws AplicacioException
    {
        
        try {
            
            e.setId(-1); //al ser nou registre, marquem id a -1 perquè sigui autoassignable
            
            if (App.getManager().isMongoDB())
            {
                 MongoDatabase conn = verificarConnexioNoSQL();
                 
                 dades.mongoDB.EquipsBD.insertEquipBD(conn, e);
                
            }else
            {
                 Connection conn = verificarConnexioSQL();
                 
                 dades.mysql.EquipsBD.insertEquipBD(conn, e);
            }
            
          
        } catch (DadesException ex) {
            throw new AplicacioException("Error a " + ex.getStackTrace()[0].getMethodName() + ":" + ex.toString());
        }
    }
    
    /***
     * Elimina un equip en funció del id
     * @param e
     * @throws AplicacioException 
     */
    public static void deleteEquip(Equip e) throws AplicacioException
    {
        
        try {
            
            if (App.getManager().isMongoDB())
            {
                MongoDatabase conn = verificarConnexioNoSQL();
                
                //verifiquem si l'equi disposa de jugadors (això no ho fa MONGODB per defecte)
                if (dades.mongoDB.JugadorsBD.CarregarJugadorsByIdEquip(conn, e.getId()).size() > 0)
                    throw new AplicacioException("No es pot eliminar l'equip: " + e.getId() + " perquè conté jugadors");
                 
                dades.mongoDB.EquipsBD.deleteEquipBD(conn, e);
            }
            else
            {
                Connection conn = verificarConnexioSQL();
                
                dades.mysql.EquipsBD.deleteEquipBD(conn, e);
            }
        } catch (DadesException ex) {
            throw new AplicacioException("Error a " + ex.getStackTrace()[0].getMethodName() + ":" + ex.toString());
        }
    }    
    
    /***
     * Elimina tots els equips
     * @throws AplicacioException Si hi ha jugadors a algun equip
     */
    public static void deleteAllEquips() throws AplicacioException
    { 
        try {
            
            if (App.getManager().isMongoDB())
            {
                MongoDatabase conn = verificarConnexioNoSQL();
                
                //abans verifiquem si hi ha algun jugador
                if (!logica.LogicJugador.getJugadors().isEmpty())
                     throw new AplicacioException("Hi ha equips amb jugadors");
                
                dades.mongoDB.EquipsBD.deleteAllEquipsBD(conn);
            }
            else
            {
                Connection conn = verificarConnexioSQL();
                
                dades.mysql.EquipsBD.deleteAllEquipsBD(conn);
            }
            
        } catch (DadesException ex) {
            throw new AplicacioException("Error a " + ex.getStackTrace()[0].getMethodName() + ":" + ex.toString());
        }
    }    
    
    /***
     * Modifica un equip amb les dades subministrades
     * @param e
     * @throws AplicacioException 
     */
    public static void modifyEquip(Equip e) throws AplicacioException
    { 
        try { 
            
            if (App.getManager().isMongoDB())
            {
                MongoDatabase conn = verificarConnexioNoSQL();
                dades.mongoDB.EquipsBD.modifyEquipBD(conn, e);
            }
            else
            {
                Connection conn = verificarConnexioSQL();
                dades.mysql.EquipsBD.modifyEquipBD(conn, e);
            }
        } catch (DadesException ex) {
            throw new AplicacioException("Error a " + ex.getStackTrace()[0].getMethodName() + ":" + ex.toString());
        }
    }
}
