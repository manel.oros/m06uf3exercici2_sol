/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import com.mongodb.client.MongoDatabase;
import main.AplicacioException;
import dades.model.Jugador;
import dades.DadesException;
import dades.mysql.JugadorsBD;
import java.sql.Connection;
import java.util.ArrayList;
import static logica.Logic.verificarConnexioNoSQL;
import static logica.Logic.verificarConnexioSQL;
import main.App;

/**
 *
 * @author manel
 */
public class LogicJugador {
    
    /***
     * Retorna una llista de jugadors en funci'de l'equip
     * @param idEquip
     * @return
     * @throws AplicacioException 
     */
    public static ArrayList<Jugador> getJugadorsByIdEquip(int idEquip) throws AplicacioException
    {
       
        ArrayList<Jugador> ret = null;
        
        try {
            if (App.getManager().isMongoDB())
            {
                 MongoDatabase conn = verificarConnexioNoSQL();
                 
                 ret = dades.mongoDB.JugadorsBD.CarregarJugadorsByIdEquip(conn, idEquip);
                 
            }
            else
            {
                Connection conn = verificarConnexioSQL();
                
                ret = dades.mysql.JugadorsBD.CarregarJugadorsByIdEquip(conn, idEquip);
            }
        }catch (DadesException ex) {
            throw new AplicacioException("Error a " + ex.getStackTrace()[0].getMethodName() + ":" + ex.toString());
        }
        return ret;
    }
    
    /***
     * Retorna tots els jugadors de la taula
     * @return
     * @throws AplicacioException 
     */
    public static ArrayList<Jugador> getJugadors() throws AplicacioException
    {
        
        ArrayList<Jugador> ret = null;
        
        try {
            if (App.getManager().isMongoDB())
            {
                MongoDatabase conn = verificarConnexioNoSQL();
                
                ret = dades.mongoDB.JugadorsBD.carregarJugadors(conn);
            }
             else
             {
                Connection conn = verificarConnexioSQL();
                
                ret = dades.mysql.JugadorsBD.CarregarJugadors(conn);
             }
        }catch (DadesException ex) {
            throw new AplicacioException("Error a " + ex.getStackTrace()[0].getMethodName() + ":" + ex.toString());
        }
        return ret;
    }
    
    /***
     * Insereix un nou jugador
     * @param j
     * @throws AplicacioException 
     */
    public static void insertJugador(Jugador j) throws AplicacioException
    {
        try {
            
            j.setId(-1); //al ser nou registre, marquem id a -1 perquè sigui autoassignable
            
            if (App.getManager().isMongoDB())
            {
                MongoDatabase conn = verificarConnexioNoSQL();
                
                dades.mongoDB.JugadorsBD.insertJugadorBD(conn, j);
            }
            else
            {
                Connection conn = verificarConnexioSQL();

                dades.mysql.JugadorsBD.insertJugadorBD(conn, j);
            }
            
        } catch (DadesException ex) {
            throw new AplicacioException("Error a " + ex.getStackTrace()[0].getMethodName() + ":" + ex.toString());
        }
    }
    
    /***
     * Elimina un jugador
     * @param j
     * @throws AplicacioException 
     */
    public static void deleteJugador(Jugador j) throws AplicacioException
    {
        try { 
            
            if (App.getManager().isMongoDB())
            {
                MongoDatabase conn = verificarConnexioNoSQL();
                
                dades.mongoDB.JugadorsBD.eliminarJugador(conn, j);
                
            }
            else
            {
                Connection conn = verificarConnexioSQL();
                
                dades.mysql.JugadorsBD.eliminarJugador(conn, j);
            }
            
        } catch (DadesException ex) {
            throw new AplicacioException("Error a " + ex.getStackTrace()[0].getMethodName() + ":" + ex.toString());
        }
    }
    
    /***
     * Elimina tots els jugadors
     * @throws AplicacioException 
     */
    public static void deleteAllJugadors() throws AplicacioException
    {
        
        try { 
            
            if (App.getManager().isMongoDB())
            {
                MongoDatabase conn = verificarConnexioNoSQL();
                
                dades.mongoDB.JugadorsBD.eliminarJugadors(conn);
                
            }
            else
            {
                Connection conn = verificarConnexioSQL();
                
                dades.mysql.JugadorsBD.eliminarJugadors(conn);
            }
            
        } catch (DadesException ex) {
           throw new AplicacioException("Error a " + ex.getStackTrace()[0].getMethodName() + ":" + ex.toString());
        }
    }
    
    /***
     * Modifica un jugador 
     * @param j
     * @throws AplicacioException 
     */
    public static void modifyJugador(Jugador j) throws AplicacioException
    {
        try {
              
            if (App.getManager().isMongoDB())
            {
                MongoDatabase conn = verificarConnexioNoSQL();
                
                dades.mongoDB.JugadorsBD.modificarJugador(conn, j);
            }
            else
            {
            
                Connection conn = verificarConnexioSQL();
            
                dades.mysql.JugadorsBD.modificarJugador(conn, j);
            }
            
        } catch (DadesException ex) {
            throw new AplicacioException("Error a " + ex.getStackTrace()[0].getMethodName() + ":" + ex.toString());
        }
    }
}
