/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacio;

import main.AplicacioException;
import logica.LogicEquip;
import logica.LogicJugador;
import dades.model.Equip;
import dades.model.Equips;
import dades.model.Jugador;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import main.App;

/**
 *
 * @author manel
 */
public class MainController extends Controller implements Initializable {

    
    //instancia on es carreguen totes les dades
    Equips dades = new Equips();
    
    //definim una llista observable d'objectes de la classe Jugador
    ObservableList<Jugador> llistaObservableJugadors;
    
    //definim una llista observable d'objectes de la classe Equip
    ObservableList<Equip> llistaObservableEquips;
    
    //atributs equips
    @FXML
    TextField txtIdEquip, txtNomEquip, txtEstadiEquip, txtPoblacioEquip, txtProvinciaEquip, txtCpEquip;
         
    //atributs jugadors
    @FXML
    TextField txtIdJugador, txtEquipJugador, txtNomJugador, txtDorsalJugador, txtEdatJugador, txtCpJugador;
    
    @FXML 
    TableView taulaJugadors;
    
    @FXML 
    TableView taulaEquips;
    
    @FXML 
    TableColumn colIdJugador, colEquipJugador, colNomJugador, colDorsalJugador, colEdatJugador, colCpJugador;
    
    @FXML 
    TableColumn colIdEquip, colNomEquip, colEstadiEquip, colPoblacioEquip, colProvinciaEquip, colCpEquip;
    
    @FXML
    private void handleOnTaulaJugadorsMouseClicked(MouseEvent event){
        Jugador jugador = (Jugador)taulaJugadors.getSelectionModel().getSelectedItem();
        
        if (jugador != null)
            setJugadorToView(jugador);
    }
    
    @FXML
    private void handleButtonModificaJugador(ActionEvent event) {
        Jugador jugador;
        
        try
        {
            jugador = this.getJugadorFromView();
            LogicJugador.modifyJugador(jugador);
            carregarJugadorsByIdEquip(jugador.getIdEquip());
            
        }
        catch (AplicacioException | NumberFormatException ex)
        {
            mostraError(ex.toString());
        }
    }
    
    @FXML
    private void handleButtonNetejaJugador(ActionEvent event) {
        clearJugadorFromView();
    }
    
    @FXML
    private void handleButtonEliminaJugador(ActionEvent event) {
        Jugador jugador;
        
        try
        {
            jugador = this.getJugadorFromView();
            LogicJugador.deleteJugador(jugador);
            carregarJugadorsByIdEquip(jugador.getIdEquip());
        }
        catch (AplicacioException | NumberFormatException ex)
        {
            mostraError(ex.toString());
        }
    }
    
     @FXML
    private void handleButtonNouJugador(ActionEvent event) {
        try
        {
            Jugador jugador = this.getJugadorFromView();
            LogicJugador.insertJugador(jugador);
            carregarJugadorsByIdEquip(jugador.getIdEquip());
        }
        catch (AplicacioException | NumberFormatException ex)
        {
            mostraError(ex.toString());
        }
    }
    
    private void carregarJugadorsByIdEquip(int idEquip)
    {
        try {
            llistaObservableJugadors = FXCollections.<Jugador>observableArrayList(LogicJugador.getJugadorsByIdEquip(idEquip));
            clearJugadorFromView();
        } catch (AplicacioException ex) {
            mostrarInfo(ex.toString());
        }
        
        llistaObservableJugadors.addListener(new ListChangeListener<Jugador>() {
        @Override public void onChanged(ListChangeListener.Change<? extends Jugador> c) {taulaJugadors.refresh();}
        });
        
        taulaJugadors.setItems(llistaObservableJugadors);
        
        if (taulaJugadors.getItems().size() > 0) 
        {
            setJugadorToView((Jugador)taulaJugadors.getItems().get(0));
        }
    }
    
     /**
     * Mostra les dades a la vista en funció del model
     * @param j 
     */
    private void setJugadorToView(Jugador j)
    {
        
        txtIdJugador.setText(j.getId().toString());
        txtNomJugador.setText(j.getNombre());
        txtEquipJugador.setText(j.getIdEquip().toString());
        txtDorsalJugador.setText(j.getDorsal().toString());
        txtEdatJugador.setText(j.getEdad().toString());
        txtCpJugador.setText(j.getCp());
    }
    
    /**
     * Neteja les dades de la vista
     */
    private void clearJugadorFromView()
    {
        txtIdJugador.setText("");
        txtNomJugador.setText("");
        txtEquipJugador.setText("");
        txtDorsalJugador.setText("");
        txtEdatJugador.setText("");
        txtCpJugador.setText("");
        
         Equip equip = (Equip)taulaEquips.getSelectionModel().getSelectedItem();
        
        if (equip != null)
        {
             txtEquipJugador.setText(equip.getId().toString());
        }
    }
    
    /**
     * 
     * @return Retorna el model en funció de la vista
     */
    private Jugador getJugadorFromView()
    {
        Jugador ret = new Jugador();
        
       
        int id = (txtIdJugador.getText().isEmpty())? -1 : Integer.valueOf(txtIdJugador.getText());
        ret.setId(id);
        ret.setNombre(txtNomJugador.getText().trim());
        ret.setIdEquip(Integer.parseInt(txtEquipJugador.getText().trim()));
        ret.setDorsal(Integer.parseInt(txtDorsalJugador.getText().trim()));
        ret.setEdad(Integer.parseInt(txtEdatJugador.getText().trim()));
        ret.setCp(txtCpJugador.getText().trim());
        
        return ret;
    }
    //</editor-fold>
    
    @FXML
    private void handleOnTaulaEquipsMouseClicked(MouseEvent event){
        Equip equip = (Equip)taulaEquips.getSelectionModel().getSelectedItem();
        
        if (equip != null)
        {
            setEquipToView(equip);
            
            carregarJugadorsByIdEquip(equip.getId());
        }
    }
    
    @FXML
    private void handleButtonCarregarEquips(ActionEvent event) {
        
        carregarEquips();
    }
    
    @FXML
    private void handleButtonNetejaEquip(ActionEvent event){
        clearEquipFromView();
    }
    
    @FXML
    private void handleButtonNetejarEquips(ActionEvent event){
        llistaObservableEquips.clear();
    }
    
    @FXML
    private void handleButtonNouEquip(ActionEvent event){
        try
        {
            Equip equip = this.getEquipFromView();
            LogicEquip.insertEquip(equip);
            carregarEquips();
        }
        catch (AplicacioException | NumberFormatException ex)
        {
            mostraError(ex.toString());
        }
    }
    
    @FXML
    private void handleButtonModificaEquip(ActionEvent event){
        Equip equip;
        
        try
        {
            equip = this.getEquipFromView();
            LogicEquip.modifyEquip(equip);
            carregarEquips();
            
        }
        catch (AplicacioException | NumberFormatException ex)
        {
            mostraError(ex.toString());
        }
    }
    
    @FXML
    private void handleButtonEliminaEquip(ActionEvent event){
        Equip equip;
        
        try
        {
            equip = this.getEquipFromView();
            LogicEquip.deleteEquip(equip);
            carregarEquips();
        }
        catch (AplicacioException | NumberFormatException ex)
        {
            mostraError(ex.toString());
        }
    }
    
    /**
     * 
     * @return Retorna el model en funció de la vista
     */
    private Equip getEquipFromView()
    {
        Equip ret = new Equip();
        
        int id = (txtIdEquip.getText().isEmpty())? -1 : Integer.valueOf(txtIdEquip.getText());

        ret.setId(id);
        ret.setNombre(txtNomEquip.getText().trim());
        ret.setEstadio(txtEstadiEquip.getText().trim());
        ret.setPoblacion(txtPoblacioEquip.getText().trim());
        ret.setProvincia(txtProvinciaEquip.getText().trim());
        ret.setCp(txtCpEquip.getText().trim());
        
        return ret;
    }
    
    /**
     * Estableix les dades d'un equip a la pantalla
     * @param e 
     */
    private void setEquipToView(Equip e)
    {
        txtIdEquip.setText(e.getId().toString());
        txtNomEquip.setText(e.getNombre());
        txtEstadiEquip.setText(e.getEstadio());
        txtPoblacioEquip.setText(e.getPoblacion());
        txtProvinciaEquip.setText(e.getProvincia());
        txtCpEquip.setText(e.getCp());
    }
    
   
    /**
     * Neteja la vista de dades
     */
    private void clearEquipFromView()
    {
        txtIdEquip.setText("");
        txtNomEquip.setText("");
        txtEstadiEquip.setText("");
        txtPoblacioEquip.setText("");
        txtProvinciaEquip.setText("");
        txtCpEquip.setText("");
    }
    
    /**
     * Carrega les dades dels equips i jugadors i els mostra a la vista
     */
    public void carregarEquips()
    {
        try {
            
            dades.setEquips(LogicEquip.getEquips());
            
            llistaObservableEquips = FXCollections.<Equip>observableArrayList(LogicEquip.getEquips());
            
            llistaObservableEquips.addListener(new ListChangeListener<Equip>() {
            @Override public void onChanged(ListChangeListener.Change<? extends Equip> c) {taulaEquips.refresh();}
            });
            
            taulaEquips.setItems(llistaObservableEquips);

            if (taulaEquips.getItems().size() > 0) 
            {
               Equip e = (Equip)llistaObservableEquips.get(0);
               carregarJugadorsByIdEquip(e.getId());
               
               taulaEquips.requestFocus();
               taulaEquips.getSelectionModel().select(0);
               taulaEquips.getFocusModel().focus(0);
            }
            
        } catch (AplicacioException ex) {
            mostraError(ex.toString());
        }
    }
    
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Altres">
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        //establim un vincle entre els atributs de la classe Jugador i les columnes del tableView
        colIdJugador.setCellValueFactory(new PropertyValueFactory<>("id"));
        colEquipJugador.setCellValueFactory(new PropertyValueFactory<>("idEquip"));
        colNomJugador.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        colDorsalJugador.setCellValueFactory(new PropertyValueFactory<>("dorsal"));
        colEdatJugador.setCellValueFactory(new PropertyValueFactory<>("edad"));
        colCpJugador.setCellValueFactory(new PropertyValueFactory<>("cp"));

        //el mateix per equips
        colIdEquip.setCellValueFactory(new PropertyValueFactory<>("id"));
        colNomEquip.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        colEstadiEquip.setCellValueFactory(new PropertyValueFactory<>("estadio"));
        colPoblacioEquip.setCellValueFactory(new PropertyValueFactory<>("poblacion"));
        colProvinciaEquip.setCellValueFactory(new PropertyValueFactory<>("provincia"));
        colCpEquip.setCellValueFactory(new PropertyValueFactory<>("cp"));
        
        //carregarEquips();
    }
    
    //</editor-fold>
            
    //<editor-fold defaultstate="collapsed" desc="Configuracio">
    @FXML
    private void handleConfigBBDD(ActionEvent event){
        
        //recupera el controlador de Login
        LoginController lc = (LoginController)App.getManager().getController(LoginController.class);
        
        //mostrem login
        Stage st = (Stage)lc.getEscena().getWindow();
        
        st.show();
        
    }
//</editor-fold>
}

