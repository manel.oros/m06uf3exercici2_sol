/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacio;

import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.Region;

/**
 * Generalitza un controlador de JavaFXML
 * @author manel
 */
public abstract class Controller {
    
    private Scene escena;
    
    public Scene getEscena() {
        return escena;
    }

    /***
     * Escena (vista) vinculada amb cada controlador
     */
    public void setEscena(Scene escena) {
        this.escena = escena;
    }
    
    /***
     * Mostra un missatge d'error JavaFX
     * @param txt Misatge a mostrar
     */
    public void mostraError(String txt)
    {
          Alert a = new Alert(Alert.AlertType.ERROR, "", ButtonType.OK);
          a.setTitle("ERROR");
          a.setContentText(txt);
          a.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
          
          //mostrem error
          a.showAndWait();
    }
    
    /**
    * Mostrem missatge amb un formulari JavaFX
    * @param txt Misatge a mostrar
    */
    public void mostrarInfo(String txt)
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Info:");
        alert.setContentText(txt);
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        
        alert.showAndWait();
    }
}
