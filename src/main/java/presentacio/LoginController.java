/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacio;

import com.mongodb.MongoException;
import com.mongodb.client.MongoDatabase;
import dades.mysql.MySQLDriver;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.App;
import dades.model.CredencialsBD;
import dades.mongoDB.MongoDriver;
import javafx.scene.control.CheckBox;

/**
 *
 * @author manel
 */
public class LoginController extends Controller implements Initializable {
    
    // model
    CredencialsBD cred = null;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
       cred = App.getManager().getCredencials();
       setDadesToView(cred);
        
    }
    
    @FXML
    private CheckBox noSQL;
    
    @FXML
    private TextField txtNomBBDD;

    @FXML
    private PasswordField txtPassword;

    @FXML
    private TextField txtUser;

    @FXML
    void handleButtonTestLogin(ActionEvent event) {
        
       test();
    }

    @FXML
    void handleButtonOkLogin(ActionEvent event) {
        
       login();
        
       tancar();
        
    }
    
    @FXML
    void handleButtonCancelLogin(ActionEvent event) {
        
        tancar();
    }
    
    @FXML
    void handleCheckBoxNoSQL(ActionEvent event) {
        
        App.getManager().setMongoDB(noSQL.isSelected());
    }
    
    /***
     * Actualitza les dades de login i mira de fer una connexió 
     * amb les credencials subministrades
     * 
     */
    private void login()
    {
        if (!App.getManager().isMongoDB())
        {
            Connection conn = null;
            cred = getDadesFromView();
            App.getManager().setCredencials(cred);
            try {
                conn = MySQLDriver.ConnectarBD(cred);
            } catch (SQLException ex) {
                mostraError("ERROR: " + ex.toString());
            }
            
            App.getManager().setConnSQL(conn);
            
        }else
        {
            MongoDatabase conn = null;
            cred = getDadesFromView();
            App.getManager().setCredencials(cred);
            try {
                conn = MongoDriver.ConnectarBD(cred);
            } catch (MongoException ex) {
                mostraError("ERROR: " + ex.toString());
            }
            
            App.getManager().setConnNoSQL(conn);
        }
        
        //mirem de refrescar les dades (si realment tenim connexió...)
        MainController mc = (MainController)App.manager.getController(MainController.class);
        mc.carregarEquips();
    }
    
    /***
     * Verifica si les dades proporcionades funcionen per a connectar
     * No actualitza cap dada ni retorna cap objecte connexió. Solament informa a l'usuari.
     */
    private void test()
    {
        if (!App.getManager().isMongoDB())
        {
            try {
               cred = getDadesFromView();
               Connection conn = MySQLDriver.ConnectarBD(cred);

               mostrarInfo("Connexió OK: " + conn.getMetaData().getURL() + " : " + conn.getMetaData().getDriverName() + " : " + conn.getMetaData().getDriverVersion());

           } catch (SQLException ex) {

                mostraError("ERROR: " + ex.toString());
           }
        }
        else
        {
            cred = getDadesFromView();
            App.getManager().setCredencials(cred);
            try {
                MongoDatabase conn = MongoDriver.ConnectarBD(cred);
                
                 mostrarInfo("Connexió OK: "  + conn.getCodecRegistry());
                
            } catch ( MongoException ex) {
                mostraError("ERROR: " + ex.toString());
            }
        }
    }
    
    /**
     * Tanca la finestra actual
     */
    private void tancar()
    {
        Stage s = (Stage)this.getEscena().getWindow();
        s.close();
    }
    
    /***
     * Retorna el model en funció de les dades de la vista
     * @return 
     */
    private CredencialsBD getDadesFromView()
    {
        CredencialsBD ret = new CredencialsBD();
        
        ret.setBD(txtNomBBDD.getText());
        ret.setPassword(txtPassword.getText());
        ret.setUser(txtUser.getText());
        
        return ret;
    }
    
    /***
     * Estableix la vista en funció del model
     * @param dades 
     */
    private void setDadesToView(CredencialsBD dades)
    {
        txtNomBBDD.setText( dades == null ? "" : dades.getBD());
        txtPassword.setText(dades == null ? "" : dades.getPassword());
        txtUser.setText(dades == null ? "" : dades.getUser());
    }

}

