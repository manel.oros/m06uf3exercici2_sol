package main;

import dades.mysql.MySQLDriver;
import presentacio.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Modality;
import presentacio.LoginController;

/**
 * JavaFX App
 */
public class App extends Application {
    
    public static Manager manager;
    
    @Override
    public void start(Stage stage)  {
        
        try {
            
            //iniciem manager
            manager = Manager.getInstance();
            
            //preparem l'escenari principal (stage)
            manager.setMainStage(stage);
            
            //inicialitzem vista/controlador i ho afegim al manager
            FXMLLoader fxmlLogin = loadFXML("VistaLoginController");
            Scene sceneLogin = new Scene(fxmlLogin.load());
            LoginController lc = fxmlLogin.getController();
            lc.setEscena(sceneLogin);
            manager.addController(lc);
            Stage stageLogin = new Stage();
            stageLogin.initModality(Modality.NONE);
            stageLogin.setTitle("Connexió a BBDD");
            stageLogin.setScene(sceneLogin);
            stageLogin.sizeToScene();

            //inicialitzem vista/controlador i ho afegim al manager
            FXMLLoader fxmlMain = loadFXML("VistaMainController");
            Scene sceneMain = new Scene(fxmlMain.load());
            MainController mc = fxmlMain.getController();
            mc.setEscena(sceneMain);
            manager.addController(mc);
            Stage stageMain = new Stage();
            stageMain.initModality(Modality.NONE);
            stageMain.setTitle("Pantalla principal");
            stageMain.setScene(sceneMain);
            stageMain.sizeToScene();
            
            // si no hi ha connexió, demanem les dades
            if ( (manager.isMongoDB() && manager.getConnNoSQL() == null) || (!manager.isMongoDB() && manager.getConnSQL() == null)) stageLogin.showAndWait();

            //mostrem pantalla principal
            stageMain.showAndWait();
            
        } catch (Exception ex) {
          
          Alert a = new Alert(Alert.AlertType.ERROR, ex.getClass().toString(), ButtonType.CLOSE);
          a.setHeaderText("No es pot iniciar l'aplicació");
          a.setContentText(ex.toString());
          
          //mostrem error
          a.show();
          
          //parem màquines
          stop();
        } 
    }

   /* public static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml).load());
    }*/

    /***
     * Carrega el fitxer FXML de la capa de presentació de resources
     * @param fxml
     * @return
     * @throws IOException 
     */
    public static FXMLLoader loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getClassLoader().getResource("presentacio/"+fxml + ".fxml"));
        return fxmlLoader;
    }

    /***
     * Punt d'entrada
     * @param args 
     */
    public static void main(String[] args) {
        launch();
    }
    
    public static Manager getManager() {
        return manager;
    }
    
    /***
     * Punt de sortida
     */
    @Override
    public void stop(){
        
        try {
            
            //tanquem la connexió, si podem
            Connection conn = manager.getConnSQL();
            if (conn != null)
                conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
           App.manager.getMainStage().close();
        }
    }
}