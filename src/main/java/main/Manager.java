/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import com.mongodb.client.MongoDatabase;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import javafx.stage.Stage;
import dades.model.CredencialsBD;
import presentacio.Controller;

/**
 * Classe Singleton que permet accedir a
 * certes dades globals de l'aplicació com els controladors, la connexió, el tipus de DB, etc...
 * @author manel
 */
public class Manager {
    
    // variable que defineix en temps de compilació si la BBDD serà MongoDB o MySQL
    private boolean mongoDB = false;
    
    private static Manager single_instance = null;
    
    /***
     * Llista de controllers de JavaFX
     */
    private final List<Controller> controllers;
    
    /***
     * Connexió a BBDD SQL
     */
    private Connection connSQL = null;
    
    
    /***
     * Connexió a BBDD MongoDB
     */
    private MongoDatabase connNoSQL = null;

   
    
    /***
     * Escenari principal de JavaFX
     */
    private Stage mainStage;
    
    /***
     * Credencials de connexió a la BBDD
     */
    private CredencialsBD credencials = null;
    
    public static Manager getSingle_instance() {
        return single_instance;
    }

    public static void setSingle_instance(Manager single_instance) {
        Manager.single_instance = single_instance;
    }

    public Stage getMainStage() {
        return mainStage;
    }

    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }

    private Manager() {
        this.controllers = new ArrayList();
        this.credencials = new CredencialsBD();
    }
    
    /***
     * Afegeix un controlador 
     * @param c 
     */
    public void addController(Controller c) {
        this.controllers.add(c);
    }
    
    /***
     * Obté un controlador amb el nom de la classe
     * @param type
     * @return 
     */
    public Controller getController(Class type) {
        Controller ret = null;
        
        for (Controller c: controllers)
        {
            if (c.getClass() == type)
                ret = c;
        }
        
        return ret;
    }
    
    /***
     * Singleton
     * @return 
     */
    public static Manager getInstance()
    {
        if (single_instance == null)
            single_instance = new Manager();
        
        return single_instance;
    }
    
    public Connection getConnSQL() {
        return connSQL;
    }

    public void setConnSQL(Connection connSQL) {
        this.connSQL = connSQL;
    }
    
    public CredencialsBD getCredencials() {
        return credencials;
    }

    public void setCredencials(CredencialsBD credencials) {
        this.credencials = credencials;
    }
    
    public MongoDatabase getConnNoSQL() {
        return connNoSQL;
    }

    public void setConnNoSQL(MongoDatabase connNoSQL) {
        this.connNoSQL = connNoSQL;
    }
    
    public boolean isMongoDB() {
        return mongoDB;
    }
    
    public void setMongoDB(boolean mongoDB) {
        this.mongoDB = mongoDB;
    }
}
